/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import com.github.javafaker.Faker;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author gohucan
 */
public class UtilsTest {
    
    private Faker faker = new Faker();
    private List<PlaceLocation> places = new ArrayList<>(100);
    private List<Place> places2 = new ArrayList<>(100);
    public UtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {

    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
            
            for (int i=0; i<100; i++){
            double latitude = Double.parseDouble(faker.address().latitude().replace(',','.'));
            double longitude = Double.parseDouble(faker.address().latitude().replace(',','.'));
            places.get(i).setLatitude(latitude);
            places.get(i).setLongitude(longitude);
            places2.get(i).setUbicacion(places.get(i));
        }
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of distance method, of class Utils.
     */
    @Test
    public void testDistance() throws IOException {
        PlaceLocation actual = null;
        try {
            actual = PlaceLocation.currentLocation();
        } catch (GeoIp2Exception ex) {
            Logger.getLogger(UtilsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        Collections.sort(places2, new ComparadorDis(actual));
        for(int i=0;i<99;i++){
            try {
                assert Utils.distance(this.places2.get(i).getUbicacion(), PlaceLocation.currentLocation()) <= Utils.distance(this.places2.get(i+1).getUbicacion(), PlaceLocation.currentLocation()) ;
            } catch (GeoIp2Exception ex) {
                Logger.getLogger(UtilsTest.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
