/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import cl.uai.oop.PlaceLocation;
import cl.uai.oop.Utils;
import cl.uai.oop.server.Main;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maurice
 */

public class ComparadorDis implements java.util.Comparator<Place> {
    PlaceLocation actual ;

    public ComparadorDis(PlaceLocation actual) {
        this.actual = actual;
    }
    
    public int compare( Place o1, Place o2) {
        double dis1 = Utils.distance(o1.getUbicacion(),actual);
        double dis2 = Utils.distance(o2.getUbicacion(),actual);
        if (dis1>dis2)return 1;
        
        else if (dis1<dis2)return -1;
        
        else return 0;

        
        }
    
    }