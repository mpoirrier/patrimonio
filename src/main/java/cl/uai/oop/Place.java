/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop;

import java.util.Date;

/**
 *
 * @author Maurice
 */
public class Place {
    protected String nombre;
    protected String descripcion;
    protected PlaceLocation ubicacion;
    protected Date apertura;
    protected Date Cierre;
    public enum tipoLugar {
    PARQUE,MUSEO,MONUMENTO
    }

    public Place(String nombre, String descripcion, PlaceLocation ubicacion, Date apertura, Date Cierre) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.ubicacion = ubicacion;
        this.apertura = apertura;
        this.Cierre = Cierre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PlaceLocation getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(PlaceLocation ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Date getApertura() {
        return apertura;
    }

    public void setApertura(Date apertura) {
        this.apertura = apertura;
    }

    public Date getCierre() {
        return Cierre;
    }

    public void setCierre(Date Cierre) {
        this.Cierre = Cierre;
    }
    
    public boolean equals(Object obj) {
        if (obj instanceof Place) {
            Place copia = (Place) obj;
            return this.nombre.compareTo(copia.nombre) == 0;
        }
        return false;
    }
}
