/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.uai.oop.server;

import cl.uai.oop.ComparadorDis;
import cl.uai.oop.PlaceLocation;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maurice
 */
public class User extends Thread {
    Socket clientSocket;
    public User(Socket clientSocket) {
        this.clientSocket=clientSocket;
    }
    public void run(){
        try{
                    InputStream is = clientSocket.getInputStream();
                    OutputStream os = clientSocket.getOutputStream();
                    DataInputStream in = new DataInputStream(is);
                    DataOutputStream out = new DataOutputStream(os);
                    String command = in.readUTF();
                    String[] parameters = command.split(";");
                    int option = Integer.parseInt(parameters[0]);
                    if (option == 1) {
                        try{
                        PlaceLocation pl = new PlaceLocation(Double.parseDouble(parameters[1]), Double.parseDouble(parameters[2]));
                        Monumentos monum= new Monumentos();
                        PlaceLocation actual = PlaceLocation.currentLocation();
                        Collections.sort(monum.listaPlace,new ComparadorDis(actual));
                        out.writeUTF(monum.listaPlace.toString());
                        out.flush();}catch(Throwable e){
                        System.out.print("Error de Calculos");
                        }
                       
                        out.close();
                        //TODO: get the nearest places sorted by distance
                        //TODO: return the list of place to client socket
                    } else if (option == 2) {
                        try{
                        String placeName = parameters[1];
                        Monumentos monum= new Monumentos();
                        int i =monum.listaPlace.indexOf(placeName);
                        out.writeUTF(monum.listaPlace.get(i).toString());
                        out.flush();}
                        catch(Throwable e){
                        System.out.print("Error de Calculos");
                        }
                        out.close();
                        //TODO: get the place using the name
                        //TODO: return the place to client socket
                    }
        } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                } 
        
        
    }
}
